# Search movies by title

<img src = "https://img.shields.io/badge/npm-5.6.0-green.svg"/> <img src = "https://img.shields.io/badge/antd-3.9.2-blue.svg"/>

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

<h1>Usage</h1>

<p>A react application which takes a movie title as search input, and returns the closest corresponding movie details.</p>
<p>Movie details include closest matching movie title, poster, year released and description.</p>

<h1>Installation</h1>

<p>Ant Design UI Library is used. You can downloaded it via npm or yarn. You can find a detailed walkthrough on how to install in [Ant Official Site](https://ant.design/docs/react/getting-started)</p>
<p> [OMDB API](http://omdbapi.com/) in json format is used to obtain movie information. You can find more information on how to use in their website.</p>

<h1>Upcoming</h1>
<ul>
<li>Add year input also</li>
<li>Make the card dissappear in start</li> (done)
</ul>
